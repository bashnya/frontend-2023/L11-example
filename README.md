# БАШНЯ. Курс по фронту. Код к занятию 11

### На занятии мы разбирали
* Что такое линтер ESLint, зачем он нужен
* Как запускать пайплайн при пуше на GitLab
* Как веб-приложения разворачиваются и хостятся с использованием Docker и Nginx

### Полезные ссылки
* [Дока ESLint](https://eslint.org/) &mdash; тут можно посмотреть все правила и
  составить свой конфиг
* [Стайтейка для расширенного понимания GitLab CI](https://habr.com/ru/articles/707112/)
* [Про Docker в подробностях](https://skillbox.ru/media/code/kak-rabotaet-docker-podrobnyy-gayd-ot-tekhlida/)
* [Инструкция с nodejs.org про запуск ноды в Docker](https://nodejs.org/ru/docs/guides/nodejs-docker-webapp)
* Запуск Nginx в Docker описан [тут](https://www.digitalocean.com/community/tutorials/how-to-run-nginx-in-a-docker-container-on-ubuntu-22-04)
* [Дока Nginx на русском](https://nginx.org/ru/docs/)
